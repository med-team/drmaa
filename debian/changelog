drmaa (0.7.9-3) unstable; urgency=medium

  [ Andreas Tille ]
  * Team upload.
  * debhelper-compat 13 (routine-update)
  * Depends: gridengine-drmaa-dev

  [ Pierre Gruet ]
  * Standards-Version: 4.6.2 (routine-update)
  * Removing unexisting packages from Suggests:

  [ Étienne Mollier ]
  * d/tests/: modified autodep8-pkg-python autopkgtest to fix jemalloc error.
    This fixes autopkgtest failure by preloading the libdrmaa.
  * d/control: remove native autodep8-pkg-python support.
  * jemalloc-5.patch: add; hint end users to preload the libdrmaa.

 -- Étienne Mollier <emollier@debian.org>  Sun, 05 Feb 2023 17:54:30 +0100

drmaa (0.7.9-2) unstable; urgency=medium

  * Team upload.
  * Add fixed path to libdrmaa.so.1.0
    Closes: #953832
  * cme fix dpkg-control
  * Add salsa-ci file (routine-update)
  * VCS fields identical to source package name

 -- Andreas Tille <tille@debian.org>  Tue, 12 May 2020 17:39:19 +0200

drmaa (0.7.9-1) unstable; urgency=medium

  * Team upload.
  * Take over maintenance in Debian Med team
  * Standards-Version: 4.5.0 (routine-update)
  * Testsuite: autopkgtest-pkg-python (routine-update)
  * Respect DEB_BUILD_OPTIONS in override_dh_auto_test target (routine-
    update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Wed, 19 Feb 2020 22:05:48 +0100

drmaa (0.7.9-0.1) experimental; urgency=medium

  * Non-maintainer upload.
  * New upstream version (Closes: #851345).
  * Drop Python 2 module package (Closes: #936444).
  * Add Python 3 module package.
  * Update debian/watch to new location.
  * Update homepage.
  * Update Vcs-* fields.
  * Update to debhelper-compat (= 12).
  * Update to pybuild.
  * Update Standards-Version to 4.4.1 (no changes required).
  * Add Recommends on appropriate drmaa library (Closes: #896391).
  * Change Priority from extra to optional.
  * Fix URLs in copyright file.
  * Add Rules-Requires-Root: no.

 -- Stuart Prescott <stuart@debian.org>  Fri, 10 Jan 2020 11:43:59 +1100

drmaa (0.5-1) unstable; urgency=low

  * new upstream release contains an important fix for
    third-party drmaa connectors (e.g. slurm-drmaa)
  * Bumped Standards-Version  (no change required)
  * switch to dh_python2

 -- Dominique Belhachemi <domibel@debian.org>  Fri, 13 Apr 2012 19:41:49 -0400

drmaa (0.4~b3-3) unstable; urgency=low

  * removed libdrmaa1.0 dependency (Closes: #648269)
  * added libdrmaa1.0 and pbs-drmaa1 to Suggests field
  * updated machine readable coyright file
  * Bumped Standards-Version  (no change required)
  * Bumped compat level (no change required)
  * added myself to Uploaders field

 -- Dominique Belhachemi <domibel@debian.org>  Thu, 10 Nov 2011 22:16:56 -0500

drmaa (0.4~b3-2) unstable; urgency=low

  * Depend on Python (Closes: #591982). Thanks to Jakub Wilk.

 -- Michael Hanke <michael.hanke@gmail.com>  Fri, 06 Aug 2010 15:43:13 -0400

drmaa (0.4~b3-1) unstable; urgency=low

  * Initial Debian packaging based on debhelper's "dh" and using the new source
    package format 3.0 (quilt) (Closes: #584436).

 -- Michael Hanke <michael.hanke@gmail.com>  Thu, 03 Jun 2010 11:41:50 -0400
